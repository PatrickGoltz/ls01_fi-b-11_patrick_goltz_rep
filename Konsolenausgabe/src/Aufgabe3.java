
public class Aufgabe3 {

	public static void main(String[] args) {
		
		
		double c2 = -28.8889;
		double c4 = -23.3333;
		double c6 = -17.7778;
		double c8 = -6.6667;
		double c10 = -1.1111;
		
		String f1 = "-20";
		String f3 = "-10";
		String f5 = "+0";
		String f7 = "+20";
		String f9 = "+30";
		
		String f = "Fahrenheit";
		String c = "Celsius";
		
		
		System.out.printf( "\n%-12s|", f );
		System.out.printf( "%10"+ "s\n", c );
		System.out.printf("-------------------------");
		System.out.printf( "\n%-12s|", f1 );
		System.out.printf( "%10.2f\n", c2);
		System.out.printf( "%-12s|", f3 );
		System.out.printf( "%10.2f\n", c4);
		System.out.printf( "%-12s|", f5 );
		System.out.printf( "%10.2f\n", c6);
		System.out.printf( "%-12s|", f7 );
		System.out.printf( "%10.2f\n", c8);
		System.out.printf( "%-12s|", f9 );
		System.out.printf( "%10.2f\n", c10);
		
	}

}
